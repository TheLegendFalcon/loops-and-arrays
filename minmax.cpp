#include <iostream>
#include <cstdlib>
#include <algorithm>
using namespace std;

int main (){
  srand((unsigned) time (0));
  int n[10];
  for (int i = 0; i < 10; i++){
    n[i] = (rand() % 100) + 0;
  }

  std::cout << "Random numbers: ";
  for(int r = 0; r < 10; r++){
    cout << n[r] << " ";
  }

  int *min = std::min_element(std::begin(n), std::end(n));
  int *max = std::max_element(std::begin(n), std::end(n));
  
  std::cout << "\nMinimum: " << *min << "\nMaximum: " << *max << "\n";

  return 0;
}
