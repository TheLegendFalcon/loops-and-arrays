#include <iostream>

int main()
{
  int x, y;
  std::cin >> x;
  std::cin >> y;

  int i, total = 0;
  for(i = x; i <= y; i++)
    {
      total = total + i;
    }
  std::cout << "Sum from " << x << " to " << y << " is " << total << "\n";
  return 0;
}
