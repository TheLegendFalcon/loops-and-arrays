#include <iostream>
#include <array>

int main(){
  int grades [100];
  int grade = 0;
  int total = 0;
  int average = 0;
  int count = 0;

  while (grade != -1){
    std::cout << "Enter grades (OR -1 TO END): ";
    std::cin >> grade;

    if (grade != -1){
      for (int i = 0; i<100; i++){
	grades[i] = grade;
	total = total + grade;
	count++;
      }

      average = total/count;
    }
  }
  std::cout << "The average score is: " << average << "\n";
}
