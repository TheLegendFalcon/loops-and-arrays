#include <iostream>

int main() {

  int n, p = 2;

  std::cout << "Please enter a digit: ";
  std::cin >> n;

  bool isPrimeNum = true;

  while (p <= n){
    for (int i=2; i <= p/2; i++){
      if (p % i == 0){
	isPrimeNum = false;
      }

      else{
	isPrimeNum = true;
      }
    }
    if (isPrimeNum == true){
      std::cout << p << " " << "\n";
    }
    p++;
  }
}
